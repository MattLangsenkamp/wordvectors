# Word Vectors

This repository contains re-implementations and re-experimentations for three important works related to
representation learning of words using neural networks.

## Installation

To run this code you will need a Java runtime installed, Scala installed and sbt installed. To manage these
requirements we recommend using [sdkman](https://sdkman.io/)

run the following commands, setting each as the default if prompted

```bash
source ~/.bashrc
sdk install scala 3.2.2
sdk install java 22.3.r19-grl
sdk install sbt 1.8.2
sbt clean compile run
```

Upon running `sbt run` you should see a welcome message 
as well as a message indicating whether the GPU is
enabled and what version of CUDA is being used. 
If the GPU was not enabled, and you want it do be, see the next section

### GPU compatibility

To get GPU compatibility working locally check what version of CUDA is
on your machine and modify the `build.sbt` appropriately based on the compatibility table located [here](https://docs.djl.ai/engines/pytorch/pytorch-engine/index.html#installation) by changing the lines 
`"ai.djl.pytorch" % "pytorch-native-cu113" % "1.11.0"` and `"ai.djl.pytorch" % "pytorch-jni" % "1.13.1-0.21.0"`.

We also provide a Dockerfile to build an image which will allow for GPU compatibility. 
To run this container you will need to have
[docker nvidia](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html) installed
```bash
docker build -t wordvectors .
docker run --rm -it --mount type=bind,source="$(pwd)",target=/opt/djl --runtime=nvidia --gpus all wordvectors bash
```
A new terminal should open, indicating that you are now operating within the container. Running main with 
```bash
sbt run
```
Should print a message indicating that the GPU is enabled.

Note that this container is bind mounted to the directory from which it was run,
meaning that work can be done within an IDE running outside the docker container, and 
it will be reflected within the container.
## Downloading Data

Each experiment within each work has its own unique data dependencies. 
These dependencies need to be downloaded prior to any training or testing. 
to download dependencies run one of the following commands:

```bash
make download-word2vec
make download-fasttext
make download-elmo
make download-all
```

Running these commands will populate the folders in `src/main/resources/data/`

## Training Models

To reproduce each experiment requires training model it depends on. 
Once the models have been trained they will reside in `src/main/resources/models/`.
Note that each of word2vec, fasttext, and elmo will require the training of multiple models.
To train the models run the following commands:

```bash 
make train-word2vec
make train-fasttext
make train-elmo
```

## Evaluation

Once the models have been produced they can be used to run the
experiments detailed in each paper. Any outputs, graphs or files
produced by the evaluation step will reside in `src/main/resources/results/`
To run the experiments run the following commands:

```bash
make evaluate-word2vec
make evaluate-fasttext
make evaluate-elmo
```