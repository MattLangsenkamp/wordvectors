package wordvectors

import ai.djl.ndarray.NDManager
import ai.djl.util.cuda.CudaUtils

@main def main(): Unit =
  val manager = NDManager.newBaseManager

  print(s"GPU enabled: ${manager.getDevice.isGpu}")
  if (manager.getDevice.isGpu) println(s" with cuda version: ${CudaUtils.getCudaVersionString}") else println()
  println("Hola, Manri")
