package wordvectors.word2vec.models

import ai.djl.Model
import ai.djl.ndarray.{NDArray, NDList, NDManager}
import ai.djl.nn.SequentialBlock
import ai.djl.nn.core.Embedding

class SkipGram(vocabSize: Int, representationSize: Int) {

  val manager: NDManager = NDManager.newBaseManager
  private val block = new SequentialBlock
  private val model: Model = Model.newInstance(s"word2vec-$representationSize")
  model.setBlock(block)

  def apply(word: NDArray): NDArray = ???

}
